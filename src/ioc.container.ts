import { Container, ContainerModule } from 'inversify';
import 'reflect-metadata';
import * as express from 'express';
import { interfaces, TYPE } from 'inversify-express-utils';
import * as basicAuth from 'express-basic-auth';

import config from './config';

import { AuthMiddleware } from './middlewares/request.auth';
import { ThrottlerMiddleware } from './middlewares/request.throttler';
import * as validation from './middlewares/request.validation';

import { AuthClientType, AuthClient, AuthClientInterface } from './services/external/auth.client';
import { VerificationClientType, VerificationClient, VerificationClientInterface } from './services/external/verify.client';
import { EmailQueueType, EmailQueueInterface, EmailQueue } from './services/queues/email.queue';
import {
  UserRepository,
  UserRepositoryInterface,
  UserRepositoryType
} from './services/repositories/user.repository';
import {
  ScreenshotRepository,
  ScreenshotRepositoryInterface,
  ScreenshotRepositoryType
} from './services/repositories/screenshot.repository';
import {
  TaskRepository,
  TaskRepositoryInterface,
  TaskRepositoryType
} from './services/repositories/task.repository';
import {
  TimepointRepository,
  TimepointRepositoryInterface,
  TimepointRepositoryType
} from './services/repositories/timepoint.repository';
import {
  ToolRepository,
  ToolRepositoryInterface,
  ToolRepositoryType
} from './services/repositories/tool.repository';
import {
  ResourceRepository,
  ResourceRepositoryInterface,
  ResourceRepositoryType
} from './services/repositories/resource.repository';
import { DummyMailService, EmailServiceInterface, EmailServiceType, NsqChannelMailService } from './services/external/email.service';
import { VerifyActionService, VerifyActionServiceType } from './services/external/verify.action.service';

import { MetricsController } from './controllers/metrics.controller';
import { ScreenshotController } from './controllers/screenshot.controller';

import { ScreenshotApplication, ScreenshotApplicationType } from './services/app/screenshot.app';

// @TODO: Moveout to file
/* istanbul ignore next */
export function buildApplicationsContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<ScreenshotApplication>(ScreenshotApplicationType).to(ScreenshotApplication);
  });
}

// @TODO: Moveout to file
/* istanbul ignore next */
export function buildServicesContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<AuthClientInterface>(AuthClientType).to(AuthClient);
    bind<VerificationClientInterface>(VerificationClientType).to(VerificationClient);
    bind<VerifyActionService>(VerifyActionServiceType).to(VerifyActionService).inSingletonScope();
    if (config.app.env === 'test') {
      bind<EmailServiceInterface>(EmailServiceType).to(DummyMailService).inSingletonScope();
    } else {
      bind<EmailServiceInterface>(EmailServiceType).to(NsqChannelMailService).inSingletonScope();
    }
    bind<EmailQueueInterface>(EmailQueueType).to(EmailQueue).inSingletonScope();

    bind<UserRepositoryInterface>(UserRepositoryType).to(UserRepository).inSingletonScope();
    bind<ScreenshotRepositoryInterface>(ScreenshotRepositoryType).to(ScreenshotRepository).inSingletonScope();
    bind<TaskRepositoryInterface>(TaskRepositoryType).to(TaskRepository).inSingletonScope();
    bind<TimepointRepositoryInterface>(TimepointRepositoryType).to(TimepointRepository).inSingletonScope();
    bind<ToolRepositoryInterface>(ToolRepositoryType).to(ToolRepository).inSingletonScope();
    bind<ResourceRepositoryInterface>(ResourceRepositoryType).to(ResourceRepository).inSingletonScope();
  });
}

// @TODO: Moveout to file
/* istanbul ignore next */
export function buildMiddlewaresContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<AuthMiddleware>('AuthMiddleware').to(AuthMiddleware).inSingletonScope();
    bind<ThrottlerMiddleware>('ThrottlerMiddleware').to(ThrottlerMiddleware).inSingletonScope();
    bind<express.RequestHandler>('MetricsBasicHttpAuth').toConstantValue(
      (req: any, res: any, next: any) => basicAuth({
        users: { [config.metrics.authUsername]: config.metrics.authPassword },
        unauthorizedResponse: 'Unauthorized',
        challenge: true
      })(req, res, next)
    );
    bind<express.RequestHandler>('VerificationRequiredValidation').toConstantValue(validation.verificationRequired);
  });
}

// @TODO: Moveout to file
/* istanbul ignore next */
export function buildControllersContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<interfaces.Controller>(TYPE.Controller).to(MetricsController).whenTargetNamed('Metrics');
    bind<interfaces.Controller>(TYPE.Controller).to(ScreenshotController).whenTargetNamed('Screenshot');
  });
}

/* istanbul ignore next */
export function buildIoc(): Container {
  const container = new Container();
  container.load(
    buildMiddlewaresContainerModule(),
    buildServicesContainerModule(),
    buildApplicationsContainerModule(),
    buildControllersContainerModule()
  );
  return container;
}

export const container = buildIoc();
