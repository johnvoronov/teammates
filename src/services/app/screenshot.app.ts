import { inject, injectable } from 'inversify';

import * as util from 'util';

import { Screenshot, ScreenshotStatuses } from '../../entities/screenshot';
import { Task, TaskStatuses } from '../../entities/task';
import { Logger } from '../../logger';
import { ScreenshotRepositoryInterface, ScreenshotRepositoryType } from '../repositories/screenshot.repository';
import { TaskRepositoryInterface, TaskRepositoryType } from '../repositories/task.repository';
import { UserRepositoryInterface, UserRepositoryType } from '../repositories/user.repository';
import { TimepointRepositoryInterface, TimepointRepositoryType } from '../repositories/timepoint.repository';
import { ToolRepositoryInterface, ToolRepositoryType } from '../repositories/tool.repository';
import { ResourceRepositoryInterface, ResourceRepositoryType } from '../repositories/resource.repository';
import { mkdir, writeFile } from 'fs';
import { User } from '../../entities/user';
import { Tool } from '../../entities/tool';
import { Resource } from '../../entities/resource';
import { Timepoint } from '../../entities/timepoint';

const writeFilePromised = util['promisify'](writeFile);
const makeDirPromised = util['promisify'](mkdir);

/**
 * UserAccountApplication
 */
@injectable()
export class ScreenshotApplication {
  private logger = Logger.getInstance('SCREENSHOT_APP');
  private tools: Tool[];
  private resources: Resource[];

  /**
   * constructor
   */
  constructor(
      @inject(ScreenshotRepositoryType) private screenshotRepository: ScreenshotRepositoryInterface,
      @inject(TaskRepositoryType) private taskRepository: TaskRepositoryInterface,
      @inject(UserRepositoryType) private userRepository: UserRepositoryInterface,
      @inject(TimepointRepositoryType) private timepointRepository: TimepointRepositoryInterface,
      @inject(ToolRepositoryType) private toolRepository: ToolRepositoryInterface,
      @inject(ResourceRepositoryType) private resourceRepository: ResourceRepositoryInterface
  ) {
  }

  async init() {
    this.tools = await this.toolRepository.getAll();
    this.resources = await this.resourceRepository.getAll();
  }

  /**
   * Set task
   * @param screenshot
   */
  async setTask(screenshot: Screenshot) {
    let tasks: Task[] = await this.taskRepository.getAllByUserAndStatus(screenshot.userId, TaskStatuses.IN_PROGRESS);
    let { data } = screenshot;
    let timepoint: Timepoint = new Timepoint(),
        task,
        tool,
        resource;

    // Try to find matches in tasks
    let processedTasks = tasks.map(task => {
      let { keys } = task;
      this.logger.debug('Got keys for task %s %o', task.name, keys);

      let matches = this.getMatches(data, keys);
      this.logger.debug('Found %o matches between screenshot %s and task %s', matches, screenshot.url, task.name);

      return { id: task.id, matches };
    });

    // Try to find matches in tools
    let processedTools = this.tools.map(tool => {
      let { keys } = tool;
      this.logger.debug('Got keys for tool %s %o', tool.name, keys);

      let matches = this.getMatches(data, keys);
      this.logger.debug('Found %o matches between screenshot %s and tool %s', matches, screenshot.url, tool.name);

      return { id: tool.id, matches };
    });

    // Try to find matches in resources
    let processedResources = this.resources.map(resource => {
      let { keys } = resource;
      this.logger.debug('Got keys for resource %s %o', resource.name, keys);

      let matches = this.getMatches(data, keys);
      this.logger.debug('Found %o matches between screenshot %s and resource %s', matches, screenshot.url, resource.name);

      return { id: resource.id, matches };
    });

    // Looking for the most likely element
    task = this.getGreatest(processedTasks);
    tool = this.getGreatest(processedTools);
    resource = this.getGreatest(processedResources);

    // Set up task id, if exists matches
    // TODO Try to find the task closest to the timepoint
    if (task.matches > 0) {
      screenshot.taskId = task.id;
      timepoint.taskId = task.id;
    }

    // Set up tool id, if exists matches
    if (tool.matches > 0) {
      timepoint.toolId = tool.id;
    }

    // Set up resource id, if exists matches
    if (resource && resource.matches > 0) {
      timepoint.resourceId = resource.id;
    }

    // Get latest timepoint for user
    let latestTimepoint = await this.timepointRepository.getLastByUser(screenshot.userId);

    // If new task id have differents, increment nonce
    let nonce = 1;

    // TODO Improve the task assignment and inheritance mechanism
    if (latestTimepoint) {
      nonce = latestTimepoint.nonce;
      if (screenshot.taskId !== latestTimepoint.taskId) {
        nonce += 1;
      }
    }

    // Create new timepoint with params
    timepoint.nonce = nonce;
    timepoint.userId = screenshot.userId;
    timepoint = await Timepoint.createTimepoint(timepoint);
    timepoint = await this.timepointRepository.save(timepoint);
    this.logger.debug('Timepoint %s was create', timepoint.id);

    // Update screenshot
    screenshot.status = ScreenshotStatuses.PROCESSED;
    screenshot = await this.screenshotRepository.save(screenshot);
    this.logger.debug('Screenshot %s was update', screenshot.url);
  }

  /**
   * Get matches between data from screenshot and keys
   *
   * @param data
   * @param keys
   */
  // TODO Add partial match processing
  getMatches(data: string, keys: Array<string>): number {
    let count = 0;
    keys.map(key => {
      let regExp = new RegExp(key, 'gi');
      let matches = data.match(regExp);

      if (matches !== null) {
        count += matches.length;
      }
    });

    return count;
  }

  /**
   * Get greatest between elements
   *
   * @param elements
   */
  getGreatest(elements) {
    if (elements.length > 0) {
      let element = elements[0];
      elements.map((item) => {
        if (item.matches > element.matches) {
          element = item;
        }
      });
      return element;
    }

    return null;
  }
}

const ScreenshotApplicationType = Symbol('ScreenshotApplicationInterface');
export { ScreenshotApplicationType };
