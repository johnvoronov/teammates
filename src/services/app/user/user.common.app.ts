import { injectable, inject } from 'inversify';
import { getConnection } from 'typeorm';
import * as bcrypt from 'bcrypt-nodejs';

import config from '../../../config';

import { User } from '../../../entities/user';
import { Logger } from '../../../logger';
import { UserRepositoryType, UserRepositoryInterface } from '../../repositories/user.repository';
import { getAllNotifications, Preferences } from '../../../entities/preferences';
import { getAllAllowedVerifications } from '../../external/verify.action.service';
import { WalletNotFound } from '../../../exceptions';

/**
 * UserCommonApplication
 */
@injectable()
export class UserCommonApplication {
  private logger = Logger.getInstance('USER_COMMON_APP');

  /**
   * constructor
   */
  constructor(
    @inject(UserRepositoryType) private userRepository: UserRepositoryInterface
  ) { }

  /**
   *
   * @param user
   */
  async getUserInfo(user: User): Promise<any> {
    // @TODO: Refactor
    const preferences = user.preferences || {};
    if (!Object.keys(preferences['notifications'] || {}).length) {
      preferences['notifications'] = getAllNotifications().reduce((p, c) => (p[c] = true, p), {});
    }
    if (!Object.keys(preferences['verifications'] || {}).length) {
      preferences['verifications'] = getAllAllowedVerifications().reduce((p, c) => (p[c] = true, p), {});
    }

    return {
      preferences,
      email: user.email,
      name: user.name,
      defaultVerificationMethod: user.defaultVerificationMethod
    };
  }
}

const UserCommonApplicationType = Symbol('UserCommonApplicationInterface');
export { UserCommonApplicationType };
