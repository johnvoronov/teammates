import { injectable } from 'inversify';
import { getManager  } from 'typeorm';

import { Timepoint } from '../../entities/timepoint';

export interface TimepointRepositoryInterface {
  getLastByUser(userId: number): Promise<Timepoint>;
  save(t: Timepoint): Promise<Timepoint>;
}

/**
 *
 */
@injectable()
export class TimepointRepository {
  getLastByUser(userId: number): Promise<Timepoint> {
    return getManager().getRepository(Timepoint).findOne({
        userId
      }, {
          order: {
            id: 'DESC'
          }
        });
  }
    /**
     *
     * @param t
     */
  save(t: Timepoint): Promise<Timepoint> {
    return getManager().getRepository(Timepoint).save(t);
  }
}

const TimepointRepositoryType = Symbol('TimepointRepositoryInterface');
export { TimepointRepositoryType };
