import { injectable } from 'inversify';
import { getManager  } from 'typeorm';

import { Screenshot, ScreenshotStatuses } from '../../entities/screenshot';

export interface ScreenshotRepositoryInterface {
  getAllByUserAndStatus(userId: number, status: ScreenshotStatuses): Promise<Screenshot[]>;
  getAllByStatus(status: ScreenshotStatuses): Promise<Screenshot[]>;
  save(s: Screenshot): Promise<Screenshot>;
}

/**
 *
 */
@injectable()
export class ScreenshotRepository {
  getAllByUserAndStatus(userId: number, status: ScreenshotStatuses): Promise<Screenshot[]> {
    return getManager().getRepository(Screenshot).find({
        userId,
        status
      });
  }

  getAllByStatus(status: ScreenshotStatuses): Promise<Screenshot[]> {
    return getManager().getRepository(Screenshot).find({
        status
      });
  }

    /**
     *
     * @param s
     */
  save(s: Screenshot): Promise<Screenshot> {
    return getManager().getRepository(Screenshot).save(s);
  }
}

const ScreenshotRepositoryType = Symbol('ScreenshotRepositoryInterface');
export { ScreenshotRepositoryType };
