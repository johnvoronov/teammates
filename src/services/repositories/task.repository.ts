import { injectable } from 'inversify';
import { getManager } from 'typeorm';

import { Task, TaskStatuses } from '../../entities/task';

export interface TaskRepositoryInterface {
  getAllByStatus(status: TaskStatuses): Promise<Task[]>;
  getAllByUserAndStatus(userId: number, status: TaskStatuses): Promise<Task[]>;
  save(t: Task): Promise<Task>;
}

/**
 *
 */
@injectable()
export class TaskRepository {
  getAllByStatus(status: TaskStatuses): Promise<Task[]> {
    return getManager().getRepository(Task).find({
        status
      });
  }

  getAllByUserAndStatus(userId: number, status: TaskStatuses): Promise<Task[]> {
    return getManager().getRepository(Task).find({
        userId,
        status
      });
  }

    /**
     *
     * @param t
     */
  save(t: Task): Promise<Task> {
    return getManager().getRepository(Task).save(t);
  }
}

const TaskRepositoryType = Symbol('TaskRepositoryInterface');
export { TaskRepositoryType };
