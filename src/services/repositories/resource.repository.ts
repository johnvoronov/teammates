import { injectable } from 'inversify';
import { getManager } from 'typeorm';

import { Resource } from '../../entities/resource';

export interface ResourceRepositoryInterface {
  getById(id: number): Promise<Resource>;
  getAll(): Promise<Resource[]>;
  save(r: Resource): Promise<Resource>;
}

/**
 *
 */
@injectable()
export class ResourceRepository {
  getById(id: number): Promise<Resource> {
    return getManager().getRepository(Resource).findOne(id);
  }

  getAll(): Promise<Resource[]> {
    return getManager().getRepository(Resource).find();
  }

  /**
   *
   * @param r
   */
  save(r: Resource): Promise<Resource> {
    return getManager().getRepository(Resource).save(r);
  }
}

const ResourceRepositoryType = Symbol('ResourceRepositoryInterface');
export { ResourceRepositoryType };
