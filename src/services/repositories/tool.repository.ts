import { injectable } from 'inversify';
import { getManager  } from 'typeorm';

import { Tool } from '../../entities/tool';

export interface ToolRepositoryInterface {
  getById(id: number): Promise<Tool>;
  getAll(): Promise<Tool[]>;
  save(t: Tool): Promise<Tool>;
}

/**
 *
 */
@injectable()
export class ToolRepository {
  getById(id: number): Promise<Tool> {
    return getManager().getRepository(Tool).findOne(id);
  }

  getAll(): Promise<Tool[]> {
    return getManager().getRepository(Tool).find();
  }

  /**
   *
   * @param t
   */
  save(t: Tool): Promise<Tool> {
    return getManager().getRepository(Tool).save(t);
  }
}

const ToolRepositoryType = Symbol('ToolRepositoryInterface');
export { ToolRepositoryType };
