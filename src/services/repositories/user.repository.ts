import { injectable } from 'inversify';
import { getManager } from 'typeorm';

import { User } from '../../entities/user';

export interface UserRepositoryInterface {
  getById(id: number): Promise<User>;
  save(u: User): Promise<User>;
}

/**
 *
 */
@injectable()
export class UserRepository {
  getById(id: number): Promise<User> {
    return getManager().getRepository(User).findOne(id);
  }

  /**
   *
   * @param u
   */
  save(u: User): Promise<User> {
    return getManager().getRepository(User).save(u);
  }
}

const UserRepositoryType = Symbol('UserRepositoryInterface');
export { UserRepositoryType };
