import * as request from 'web-request';
import { injectable } from 'inversify';
import * as QR from 'qr-image';

import config from '../../config';
import {
  MaxVerificationsAttemptsReached,
  NotCorrectVerificationCode,
  VerificationIsNotFound
} from '../../exceptions';

/**
 *
 */
export interface VerificationClientInterface {
  initiateVerification(method: string, data: InitiateData): Promise<InitiateResult>;
  validateVerification(method: string, id: string, input: ValidateVerificationInput): Promise<ValidationResult>;
  invalidateVerification(method: string, id: string): Promise<void>;
  getVerification(method: string, id: string): Promise<ValidationResult>;
}

/* istanbul ignore next */
@injectable()
export class VerificationClient implements VerificationClientInterface {
  tenantToken: string;
  baseUrl: string;

  constructor(baseUrl: string = config.verify.baseUrl) {
    request.defaults({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      throwResponseError: true
    });

    this.baseUrl = baseUrl;
    this.tenantToken = config.auth.token;
  }

  async initiateVerification(method: string, data: InitiateData): Promise<InitiateResult> {
    const result = await request.json<InitiateResult>(`/methods/${method}/actions/initiate`, {
      baseUrl: this.baseUrl,
      auth: {
        bearer: this.tenantToken
      },
      method: 'POST',
      body: data
    });

    result.method = method;
    delete result.code;
    if (result.totpUri) {
      const buffer = QR.imageSync(result.totpUri, {
        type: 'png',
        size: 20
      });
      result.qrPngDataUri = 'data:image/png;base64,' + buffer.toString('base64');
    }

    return result;
  }

  async validateVerification(method: string, id: string, input: ValidateVerificationInput): Promise<ValidationResult> {
    try {
      return await request.json<ValidationResult>(`/methods/${method}/verifiers/${id}/actions/validate`, {
        baseUrl: this.baseUrl,
        auth: {
          bearer: this.tenantToken
        },
        method: 'POST',
        body: input
      });
    } catch (e) {
      if (e.statusCode === 422) {
        if (e.response.body.data.attempts >= config.verify.maxAttempts) {
          await this.invalidateVerification(method, id);
          throw new MaxVerificationsAttemptsReached('You have used all attempts to enter code');
        }

        throw new NotCorrectVerificationCode('Not correct code');
      }

      if (e.statusCode === 404) {
        throw new VerificationIsNotFound('Code was expired or not found. Please retry');
      }

      throw e;
    }
  }

  async invalidateVerification(method: string, id: string): Promise<void> {
    await request.json<Result>(`/methods/${method}/verifiers/${id}`, {
      baseUrl: this.baseUrl,
      auth: {
        bearer: this.tenantToken
      },
      method: 'DELETE'
    });
  }

  async getVerification(method: string, id: string): Promise<ValidationResult> {
    try {
      return await request.json<ValidationResult>(`/methods/${method}/verifiers/${id}`, {
        baseUrl: this.baseUrl,
        auth: {
          bearer: this.tenantToken
        },
        method: 'GET'
      });
    } catch (e) {
      if (e.statusCode === 404) {
        throw new VerificationIsNotFound('Code was expired or not found. Please retry');
      }

      throw e;
    }
  }
}

const VerificationClientType = Symbol('VerificationClientInterface');
export { VerificationClientType };

declare interface RegistrationResult {
  id: string;
  email: string;
  login: string;
}

declare interface TenantRegistrationResult extends RegistrationResult {

}

declare interface UserRegistrationResult extends RegistrationResult {
  tenant: string;
  sub: string;
  scope?: any;
}

declare interface VerificationResult {
  id: string;
  login: string;
  jti: string;
  iat: number;
  aud: string;
}

declare interface TenantVerificationResult extends VerificationResult {
  isTenant: boolean;
}

declare interface UserVerificationResult extends VerificationResult {
  deviceId: string;
  sub: string;
  exp: number;
  scope?: any;
}

declare interface UserVerificationResponse {
  decoded: UserVerificationResult;
}

declare interface TenantVerificationResponse {
  decoded: TenantVerificationResult;
}

declare interface AuthUserData {
  email: string;
  login: string;
  password: string;
  sub: string;
  scope?: any;
}

declare interface UserLoginData {
  login: string;
  password: string;
  deviceId: string;
}

declare interface AccessTokenResponse {
  accessToken: string;
}

declare interface InitiateData {
  consumer: string;
  issuer?: string;
  template?: {
    body: string;
    fromEmail?: string;
    subject?: string;
  };
  generateCode?: {
    length: number;
    symbolSet: Array<string>;
  };
  policy: {
    expiredOn: string;
  };
  payload?: any;
}

declare interface Result {
  status: number;
}

declare interface InitiateResult extends Result {
  verificationId: string;
  attempts: number;
  expiredOn: number;
  method: string;
  code?: string;
  totpUri?: string;
  qrPngDataUri?: string;
}

declare interface ValidationResult extends Result {
  data?: {
    verificationId: string;
    consumer: string;
    expiredOn: number;
    attempts: number;
    payload?: any;
  };
}

declare interface InitiatedVerification {
  verificationId: string;
  method: string;
  totpUri?: string;
  qrPngDataUri?: string;
}

declare interface ValidateVerificationInput {
  code: string;
  removeSecret?: boolean;
}

declare interface UserData {
  email: string;
  name: string;
  agreeTos: boolean;
  passwordHash?: string;
  source?: any;
  wallets?: any[];
}

declare interface InputUserData extends UserData {
  password: string;
  paymentPassword: string;
}

declare interface Wallet {
  ticker: string;
  address: string;
  balance: string;
  tokens: any[];
  name: string;
  color: number;
  salt?: string;
}

declare interface NewWallet extends Wallet {
  privateKey: string;
  mnemonic: string;
}

declare interface InputWallet {
  address?: string;
  name?: string;
  color?: number;
}

declare interface CreatedUserData extends UserData {
  isVerified: boolean;
  defaultVerificationMethod: string;
  verification: InitiatedVerification;
}

declare interface BaseInitiateResult {
  verification: InitiatedVerification;
}

declare interface InitiateLoginResult extends BaseInitiateResult {
  accessToken: string;
  isVerified: boolean;
}

declare interface VerifyLoginResult extends InitiateLoginResult {
}

declare interface VerificationData {
  verificationId: string;
  code: string;
}

declare interface VerificationInput {
  verification: VerificationData;
}

declare interface ActivationUserData {
  email: string;
  verificationId: string;
  code: string;
}

declare interface ActivationResult {
  accessToken: string;
}

declare interface InitiateLoginInput {
  email: string;
  password: string;
}

declare interface VerifyLoginInput extends VerificationInput {
  accessToken: string;
}

declare interface ResetPasswordInput {
  email: string;
}

declare interface InitiateChangePasswordInput {
  oldPassword: string;
  newPassword: string;
}

declare interface Enable2faResult {
  enabled: boolean;
}

declare interface UserInfo {
  ethAddress: string;
  tokens: any;
  email: string;
  name: string;
  defaultVerificationMethod: string;
}

declare interface TransactionInput {
  from: string;
  to: string;
  amount: string;
  gas: string;
  gasPrice: string;
  data?: any;
}

declare interface DeployContractInput {
  constructorArguments: any;
  byteCode: string;
  gasPrice: string;
  gas?: string;
}

declare interface ExecuteContractConstantMethodInput {
  methodName: string;
  arguments: any;
  gasPrice: string;
}

declare interface ExecuteContractMethodInput extends ExecuteContractConstantMethodInput {
  amount: string;
  gas?: string;
}

declare interface RemoteInfoRequest {
  app: {
    locals: {
      remoteIp: string;
    }
  };
}

declare interface ReqBodyToInvestInput {
  gas: string;
  gasPrice: string;
  ethAmount: string;
}

declare interface Erc20TokenInfo {
  name: string;
  symbol: string;
  decimals: number;
}
