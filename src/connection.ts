import 'reflect-metadata';
import { createConnection, Connection, ConnectionOptions } from 'typeorm';
import { Screenshot } from './entities/Screenshot';

// TODO change settings
const connectionOpts: ConnectionOptions = {
  type: 'mongodb',
  host: process.env.DB_HOST || 'localhost',
  port: Number(process.env.DB_PORT) || 27017,
  username: process.env.DB_USERNAME || '',
  password: process.env.DB_PASSWORD || '',
  database: process.env.DB_NAME || 'teammates',
  entities: [
    Screenshot
  ],
  synchronize: true,
  logging: false
};

const connection: Promise<Connection> = createConnection(connectionOpts);

export default connection;
