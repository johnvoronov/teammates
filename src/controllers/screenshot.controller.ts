import * as Joi from 'joi';
import { Response, Request } from 'express';
import { inject, injectable } from 'inversify';
import { controller, httpPost, httpGet, httpPut } from 'inversify-express-utils';

import { responseWith } from '../helpers/responses';
import { AuthenticatedRequest } from '../interfaces';

import { ScreenshotApplicationType, ScreenshotApplication } from '../services/app/screenshot.app';
import { commonFlowRequestMiddleware, verificationValidateSchema } from '../middlewares/request.validation';
import { Screenshot } from '../entities/screenshot';

/**
 * UserController
 */
/* istanbul ignore next */
@controller(
  '/screenshot',
  'ThrottlerMiddleware'
)
export class ScreenshotController {
  constructor(
    @inject(ScreenshotApplicationType) private screenshotApp: ScreenshotApplication
  ) { }

  /**
   *
   * @param req
   * @param res
   */
  @httpPut(
    '/',
    (req, res, next) => {
      commonFlowRequestMiddleware(Joi.object().keys({
        screenshot: Joi.string().required()
      }), req, res, next);
    }
  )
  async setTask(req: Request & AuthenticatedRequest, res: Response): Promise<void> {
    let result = await this.screenshotApp.setTask(req.body.screenshot);
    res.json(result);
  }
}
