import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { AuthenticatedRequest } from './interfaces';
import { CronJob } from 'cron';

import { createConnection, ConnectionOptions } from 'typeorm';

import { ScreenshotRepositoryInterface, ScreenshotRepositoryType } from './services/repositories/screenshot.repository';
import { UserRepositoryInterface, UserRepositoryType } from './services/repositories/user.repository';
import { TaskRepositoryInterface, TaskRepositoryType } from './services/repositories/task.repository';

import { User } from './entities/user';
import { Task, TaskStatuses } from './entities/task';

import { ScreenshotApplicationType, ScreenshotApplication } from './services/app/screenshot.app';
import { Screenshot, ScreenshotStatuses } from './entities/screenshot';
import { Logger } from './logger';

import config from './config';
import { HttpServer } from './http.server';
import { container } from './ioc.container';

/**
 * ScreenshotDaemon
 */
export class ScreenshotDaemon {
  private logger = Logger.getInstance('SCREENSHOT_DAEMON');
  private job: CronJob;
  private static userRepository: UserRepositoryInterface;
  private static screenshotRepository: ScreenshotRepositoryInterface;
  private static screenshotApp: ScreenshotApplication;
  private static taskRepository: TaskRepositoryInterface;

  constructor(
        @inject(UserRepositoryType) userRepository: UserRepositoryInterface,
        @inject(ScreenshotRepositoryType)  screenshotRepository: ScreenshotRepositoryInterface,
        @inject(TaskRepositoryType)  taskRepository: TaskRepositoryInterface,
        @inject(ScreenshotApplicationType) screenshotApp: ScreenshotApplication
    ) {
    ScreenshotDaemon.userRepository = userRepository;
    ScreenshotDaemon.screenshotRepository = screenshotRepository;
    ScreenshotDaemon.taskRepository = taskRepository;
    ScreenshotDaemon.screenshotApp = screenshotApp;
  }

  async init(): Promise<any> {

    createConnection(config.typeOrm as ConnectionOptions).then(async connection => {
         await this.cronSubscribe();
       });
  }

  cronSubscribe(): void {

    this.job = new CronJob(
            '*/5 * * * * *',
            this.cronJob,
            null,
            true,
            'America/Los_Angeles'
        );

    this.logger.debug('Cron was started');
  }

  async cronJob() {
    let screenshotItems = await ScreenshotDaemon.screenshotRepository.getAllByStatus(ScreenshotStatuses.PENDING);
        // TODO Add queue
    screenshotItems.map(async screenshot => {
         await ScreenshotDaemon.screenshotApp.init();
         await ScreenshotDaemon.screenshotApp.setTask(screenshot);
            // let user: User = await ScreenshotDaemon.userRepository.getById(item.userId);
          //  let taskItems = await ScreenshotDaemon.taskRepository.getAllByUserAndStatus(item.userId, TaskStatuses.IN_PROGRESS);
          //  console.log(taskItems)
       });
        //let result = await this.screenshotApp.setTask(req.body.screenshot);await this.setTask();
  }
}
//console.log(container.get(ScreenshotRepositoryType))
let daemon = new ScreenshotDaemon(
    container.get(UserRepositoryType),
    container.get(ScreenshotRepositoryType),
    container.get(TaskRepositoryType),
    container.get(ScreenshotApplicationType)
);
daemon.init();
