import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Verifications, VerifyMethod } from '../services/external/verify.action.service';
import { Preferences, Notifications } from './preferences';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  //@Column(type => Team)
  //public team: Team;

  //@Column(type => Preferences)
  //preferences: Preferences;

  @Column()
  email: string;

  @Column()
  name: string;

  @Column()
  passwordHash: string;

  @Column()
  agreeTos: boolean;

  @Column()
  isVerified: boolean;

  @Column()
  defaultVerificationMethod: string;

  @Column()
  source: string;

  @Column()
  securityKey: string;

  @Column()
  recoveryKey: string;

  @Column()
  salt: string;

  @Column()
  mnemonic: string;

  @Column()
  createdAt: number;

  static createUser(data: any) {
    const user = new User();
    user.securityKey = data.securityKey;
    user.recoveryKey = data.recoveryKey;
    user.salt = data.salt;
    user.mnemonic = data.mnemonic;
    user.email = data.email;
    user.name = data.name;
    user.agreeTos = data.agreeTos || false;
    user.passwordHash = data.passwordHash;
    user.isVerified = false;
    user.defaultVerificationMethod = VerifyMethod.EMAIL;
    user.source = data.source || 'unknown';
    user.createdAt = ~~(+new Date() / 1000);
    //user.preferences = new Preferences();
    return user;
  }

  /*isNotificationEnabled(notification: Notifications): boolean {
    return !this.preferences || !this.preferences.notifications ||
      this.preferences.notifications[notification] ||
      this.preferences.notifications[notification] === undefined;
  }

  isVerificationEnabled(verification: Verifications): boolean {
    return !this.preferences || !this.preferences.verifications ||
      this.preferences.verifications[verification] ||
      this.preferences.verifications[verification] === undefined;
  }*/
}
