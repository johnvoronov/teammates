import { Column, Entity, JoinColumn, ObjectID, ManyToOne, ObjectIdColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Index } from 'typeorm/decorator/Index';
import { base64encode } from '../helpers/helpers';
import { Verifications, VerifyMethod } from '../services/external/verify.action.service';
import { User } from './user';

export enum TaskStatuses {
  TODO = 'ToDo',
  IN_PROGRESS = 'In Progress',
  INFORMATION_REQUEST = 'Information Request',
  DONE = 'DONE'
}

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int', { nullable: true })
  userId: number;

  @Column()
  source: string; // TODO

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  branch: string;

  @Column('text', {
    array: true
  })
  keys: string[];

  @Column()
  status: TaskStatuses;

  @Column()
  createdAt: number;

  static createTask(data: any) {
    const task = new Task();
    task.userId = data.userId;
    task.source = data.source;
    task.name = data.name;
    task.description = data.description;
    task.branch = data.branch;
    task.status = data.status;
    task.keys = data.keys;
    task.createdAt = ~~(+new Date() / 1000);
    return task;
  }
}
