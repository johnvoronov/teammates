import { Column, PrimaryGeneratedColumn, Entity, ObjectID, ManyToOne, ObjectIdColumn, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user';
import { Task } from './task';
import { VerifyMethod } from '../services/external/verify.action.service';

export enum ScreenshotStatuses {
  PENDING = 'pending',
  PROCESSED = 'processed',
  FAILED = 'failed'
}

@Entity()
export class Screenshot {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int', { nullable: true })
  userId: number;

  @Column('int', { nullable: true })
  taskId: number;

  @Column()
  url: string;

  @Column()
  data: string;

  @Column()
  createdAt: number;

  @Column()
  status: ScreenshotStatuses;

  static createScreenshot(data: any) {
    const screenshot = new Screenshot();
    screenshot.userId = data.userId;
    screenshot.taskId = data.taskId;
    screenshot.url = data.url;
    screenshot.data = data.data;
    screenshot.status = data.status;
    screenshot.createdAt = ~~(+new Date() / 1000);
    return screenshot;
  }
}
