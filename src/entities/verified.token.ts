import { Column, Entity, JoinColumn, ObjectID, ObjectIdColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user';

@Entity()
export class VerifiedToken {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int', { nullable: true })
  userId: number;

  @Column()
  token: string;

  @Column()
  verified: boolean;

  @Column()
  createdAt: number;

  static createNotVerifiedToken(user: User, token: string) {
    const verifiedToken = new VerifiedToken();
    verifiedToken.userId = user.id;
    verifiedToken.createdAt = ~~(+new Date() / 1000);
    verifiedToken.token = token;
    verifiedToken.verified = false;
    return verifiedToken;
  }

  static createVerifiedToken(user: User, token: string) {
    const verifiedToken = new VerifiedToken();
    verifiedToken.userId = user.id;
    verifiedToken.createdAt = ~~(+new Date() / 1000);
    verifiedToken.token = token;
    verifiedToken.verified = true;
    return verifiedToken;
  }

  makeVerified() {
    if (this.verified) {
      throw Error('Token is verified already');
    }

    this.verified = true;
  }
}
