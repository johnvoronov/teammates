import { Column, PrimaryGeneratedColumn, Entity, ObjectID, ManyToOne, ObjectIdColumn, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user';
import { Task, TaskStatuses } from './task';
import { VerifyMethod } from '../services/external/verify.action.service';

export enum TimepointStatuses {
  PENDING = 'pending',
  PROCESSED = 'processed',
  FAILED = 'failed'
}

@Entity()
export class Timepoint {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int', { nullable: true })
  userId: number;

  @Column('int', { nullable: true })
  taskId: number;

  @Column('int', { nullable: true })
  taskStatusId: number;

  @Column('int', { nullable: true })
  toolId: number;

  @Column('int', { nullable: true })
  resourceId: number;

  @Column({ nullable: false })
  nonce: number;

  @Column()
  createdAt: number;

  static createTimepoint(data: any) {
    const timepoint = new Timepoint();
    timepoint.userId = data.userId;
    timepoint.taskId = data.taskId;
    timepoint.taskStatusId = data.taskStatusId;
    timepoint.toolId = data.toolId;
    timepoint.resourceId = data.resourceId;
    timepoint.nonce = data.nonce;
    timepoint.createdAt = ~~(+new Date() / 1000);
    return timepoint;
  }
}
