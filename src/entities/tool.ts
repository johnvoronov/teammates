import { Column, PrimaryGeneratedColumn, Entity, ObjectID, ManyToOne, ObjectIdColumn, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user';
import { Task, TaskStatuses } from './task';
import { VerifyMethod } from '../services/external/verify.action.service';

export enum ToolTypes {
  DESIGN = 'design',
  LAYOUT = 'layout',
  DEVELOPMENT = 'development',
  TESTING = 'testing',
  DEVOPS = 'devops',
  OTHER = 'other'
}

@Entity()
export class Tool {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('text', {
    array: true
  })
  keys: string[];

  @Column()
  image: string;

  @Column({ type: 'enum', enum: ToolTypes })
  type: ToolTypes;

  @Column()
  resource: boolean;

  @Column()
  createdAt: number;

  static createTool(data: any) {
    const tool = new Tool();
    tool.name = data.name;
    tool.keys = data.keys;
    tool.image = data.image;
    tool.type = data.type;
    tool.createdAt = ~~(+new Date() / 1000);
    return tool;
  }
}
