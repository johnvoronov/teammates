import { Column, PrimaryGeneratedColumn, Entity, ObjectID, ManyToOne, ObjectIdColumn, OneToOne, JoinColumn } from 'typeorm';
import { User } from './user';
import { Task, TaskStatuses } from './task';
import { VerifyMethod } from '../services/external/verify.action.service';

@Entity()
export class Resource {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('text', {
    array: true
  })
  keys: string[];

  @Column()
  url: string;

  @Column()
  image: string;

  @Column()
  createdAt: number;

  static createResource(data: any) {
    const resource = new Resource();
    resource.name = data.name;
    resource.keys = data.keys;
    resource.url = data.url;
    resource.image = data.image;
    resource.createdAt = ~~(+new Date() / 1000);
    return resource;
  }
}
