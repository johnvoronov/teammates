import { createLogger, format, transports } from 'winston';
const { combine, label, printf, timestamp, splat, json } = format;

let options = {
  slack: {
    webhook: `https://hooks.slack.com/services/${process.env.SLACK_TOKEN}`,
    channel: '#notify',
    level: 'error',
    username: 'bot'
  },
  finance: {
    webhook: `https://hooks.slack.com/services/${process.env.FINANCE_SLACK_TOKEN}`,
    channel: '#finance',
    level: 'finance',
    username: 'bot'
  },
  all: {
    level: process.env.LOGGER_LEVEL
  },
  files: {
    dirname: 'logs',
    filename: `zpay-${process.env.SERVICE_NAME}-%DATE%.log`,
    datePattern: 'DD-MM-YYYY',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d'
  }
};

let myFormat = printf((info) => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const customLevels = {
  levels: {
    finance: 0,
    auth: 1,
    error: 2,
    slack: 3,
    warn: 4,
    info: 5,
    verbose: 6,
    debug: 7,
    silly: 8
  }
};

export function newConsoleTransport(name?: string) {
  return new (transports.Console)();
}

const EXCEPTION_CAPTION = 'Throwed exception: ';

/**
 * Simple logger with tags.
 */
export interface SubLogger {
  addPrefix(text): SubLogger;
  addMeta(meta: { [k: string]: any; }): SubLogger;
  debug(msg: string, ...meta: any[]);
  verbose(msg: string, ...meta: any[]);
  info(msg: string, ...meta: any[]);
  warn(msg: string, ...meta: any[]);
  error(msg: string, ...meta: any[]);
  exception(...meta: any[]);
}

export const loggerOld = createLogger({
  format: combine(
        json(),
        label({ label: 'core' }), // config.service.name
        splat(),
        timestamp(),
        myFormat
    ),
  exitOnError: false,
  levels: customLevels.levels,
  level: 'silly',// TODO Change to config
  transports: [
    new transports.Console({ level: 'silly' })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  loggerOld.add(new transports.Console({
    format: format.combine(
            format.colorize(),
            format.simple()
        )
  }));
}
